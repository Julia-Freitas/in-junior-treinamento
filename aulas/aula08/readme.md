# Aula 08 - CSS dia 2

# Tarefa 

#### Estudar conteúdos:

Flex:
https://origamid.com/projetos/flexbox-guia-completo/
https://css-tricks.com/snippets/css/a-guide-to-flexbox/
https://flexboxfroggy.com/

Grid:
https://www.origamid.com/projetos/css-grid-layout-guia-completo/
https://css-tricks.com/snippets/css/complete-guide-grid/
https://cssgridgarden.com/