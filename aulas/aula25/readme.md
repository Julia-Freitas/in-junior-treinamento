# Aula 25 - Bootstrap

### Tarefa

Tarefa será apenas vocês desenvolverem uma página como eu fiz em aula e colocar em prática o uso dos componentes do framework. Estão livres utilizarem e estilizarem como quiser e a temática da página é livre também.

Como vocês possuem tarefas de Ruby e de Wordpress, digo que elas tem maior importância já que o conteúdo terá continuidade na aula de amanhã! Por isso, o prazo de entrega é até sexta-feira (14/08) às 23:59