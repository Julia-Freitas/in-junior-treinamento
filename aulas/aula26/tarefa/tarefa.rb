
class Usuarios(email, senha, nome, nascimento, logado)

    @email = email
    @senha = senha
    @nome = nome
    @nascimento = nascimento
    @logado = logado

    def idade
        @idade
    end

    # muda estado para ativo
    def logar(senha)
        if (senha == @senha)
            logado = true
    end

    #muda estado para inativo
    def not_logar
        logado = false
    end

end

class Aluno(matricula, periodo_letivo, curso)

    @matricula = matricula   
    @periodo_letivo = periodo_letivo
    @curso = curso
    @turmas = []


    # Adiciona a turma do aluno no array de turmas
    def inscrever(nome_turma)
        @turmas.push(nome_turma)
    end




class Turma(nome:, horario:, dias_semana, nome_aluno, inscricao_aberta)

    @nome = nome
    @horario = horario
    @dias_semana = []
    @inscritos = []
    @inscricao_aberta = inscricao_aberta


    # muda estado para ativo
    def abrir_inscricao
        @inscricao_aberta = true
    end

    #muda estado para inativo
    def fechar_inscricao
        @inscricao_aberta = false
    end

    # adiciona aluno no array inscritos
    def adicionar_aluno(nome_aluno)
        @inscritos.push(nome_aluno)
    end
    
end


class Professor(matricula, salario, nome_materia)

    @matricula = matricula
    @salario = salario
    materias = []

    # adiciona materia do argumento no array de materias
    def adicionar_materia(nome_materia)
        @materias.push(nome_aluno)
    end

end

class Materia(ementa, nome, nome_professor)

    @ementa = ementa
    @nome = nome
    professores = []

    #adiciona professor ao array de professores
    def adicionar_professor(nome_professor)
        @professores.push(nome_professor)
    end
end