# Aula 13 - Criação


### Noções de Design

 cores complementares: cores de contraste máximo

 cores análogas: cores que estão lado a lado, no circulo cromatico

 harmonia triádica (o mais utilizado): meio termo entre o complementar e o análogo - á contraste de cores, mas balanceado, sem causar 

 --------------------------------

### Tipografia

Serifa: 

Sem serifa: fontes neutras. Usadas para compor Seus traços retos são harmonizados com formas geométricas


### Garantindo boa legibilidade

Altura x(média) ou alta
Peso
Largura da letra
Contraste pequeno entre a largura dos traços


Fontes Decorativas

Script: São elegantes e estilosas, mas não possuem boa legibilidade, geralmente associadas a eventos.

Display: Categoria "outros". Geralmente são feitas sob medida para um projeto específico.


### Tarefa

Fazer um layout no Figma com os (requisitos)
(https://drive.google.com/drive/u/2/folders/1DWm_KuWy3dY1ZiBidQKbl1JNvHtlusFn)



!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

A tarefa pode ser entregue até domingo 23:59

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!