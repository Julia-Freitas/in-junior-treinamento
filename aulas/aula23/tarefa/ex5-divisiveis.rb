#Crie uma função que dado um array de inteiros, retorne um array apenas com os divisíveis por 3.
#Ex: Entrada: [3, 6, 7, 8]
#    Saída: [3, 6]


# percorre o array inicial, iterando
# a cada numero iterado, comparo se eh divisivel por 3
# adiciona no newarray, somente os divisiveis por 3
array = [3, 6, 7, 8]
newarray = []
numero = 0
array.each do |numero|
    if ((numero % 3) == 0 )
        newarray.push(numero)
    end
end

puts "\n" + "newarray: " + newarray.to_s + "\n\n"