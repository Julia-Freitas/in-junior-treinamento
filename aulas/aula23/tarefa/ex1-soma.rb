#Crie uma função que dado um array de arrays, imprima na tela a soma e a multiplicação de todos os valores. 
#Ex: Entrada: [[2, 5, 7], [3, 2, 4, 10], [1, 2, 3]]
#    Impressão na tela: Soma: 39
#    Impressão na tela: Multiplicação: 100800


array = [[2, 5, 7], [3, 2, 4, 10], [1, 2, 3]]
numero, soma = 0

array.each do |numero|
        soma = soma + numero
        puts numero
end
puts soma


=begin
teste = [[2, 5], 7]
puts teste
teste.each do |elemento|
    puts elemento
end

teste.each do |numero|
    soma += numero
    puts soma
end
=end