#Crie uma função que dado dois valores, retorne a divisão deles (a divisão deve retornar um float).
#Ex: Entrada: 5, 2
#    Saída: 2.5

# entrada do teclado
puts "digite primeiro numero"    
el1 = gets
puts "digite segundo numero" 
el2 = gets

# calculo da divisao: primeiro converte os dois numeros para float e faz a divisao, que tambem retorna um float
resp = Float(el1)/Float(el2)

#imprime resposta
puts "Resposta da divisão: " + resp.to_s