#Crie uma função que dado um array de inteiros, faça um Cast para String e retorne um array com os valores em string.
#Ex: Entrada: [25, 35, 45]
#    Saída: ["25", "35", "45"]




# percorre o array inicial e adiciona o elementos convertidos em string, no newarray
array = [25, 35, 45]
newarray = []

# percorre array
array.each do |numero|
    # converte os elementos do array em string e insere no newarray 
    newarray.push(numero.to_s)
end

puts "\n" + "newarray: " + newarray.to_s + "\n\n"
