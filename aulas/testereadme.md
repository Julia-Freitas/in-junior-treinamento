<!DOCTYPE html>
<html>
	<head>
	<style>
	table {
	  font-family: arial, sans-serif;
	  border-collapse: collapse;
	  width: 100%;
	}

	td, th {
	  border: 1px solid #dddddd;
	  text-align: center;
	  padding: 8px;
	}

	tr:nth-child(even) {
	  background-color: #dddddd;
	}
	</style>
	</head>

<body>

	<h1> Treinamento IN Junior </h1>
	
	<h2> Aulas </h2>
	
	<table>
		<!-- linha -->
		<tr>
			<!-- colunas do header -->
			<th> Aula </th>
			<th> Repositório </th>
			<th> Vídeoaula </th>
			<th> Feedback </th>
			<th> Tarefa </th>
		</tr>

		<tr>
			<!-- colunas -->
			<td> Formação de Equipes </td>
			<td> <a href=''> Repositório 	</a></td>
			<td> <a href=''> Vídeoaula 		</a></td>
			<td> &#9989 </td>
			<td> </td>
		</tr>
		
		
		<tr>
			<!-- colunas -->
			<td> Git </td>
			<td> Repositório </td>
			<td> Vídeoaula </td>
		</tr>
		
		
		<tr>
			<!-- colunas -->
			<td> Aula 0 </td>
			<td> Repositório </td>
			<td> Vídeoaula </td>
		</tr>
		
		
		<tr>
			<!-- colunas -->
			<td> Aula 0 </td>
			<td> Repositório </td>
			<td> Vídeoaula </td>
		</tr>
		
		
		<tr>
			<!-- colunas -->
			<td> Aula 0 </td>
			<td> Repositório </td>
			<td> Vídeoaula </td>
		</tr>
		
		
		
		

	</table>

</html>
