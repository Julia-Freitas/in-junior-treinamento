# Aula 07 - JavaScript - dia 2


##### Instrutor: Thiago
##### Monitores: Arthur Nakao

### Ex de aula:

/* 
    Fazer uma função que retorne um array sem elementos duplicados
    ex: [1, 2, 2, 3, 7, 4, 3, 2, 1] => [1, 2, 3, 4, 7]
/

/
    Fazer uma função que retorne verdadeiro se um array possui os mesmos valores, independente da ordem
    ex: [1, 1, 2, 3] [2, 3, 1, 1] => true
        [1, 1, 3, 2] [3, 1 , 2, 2] => false
/

/
    Fazer uma função que retorne um array com a soma dos valores em posições iguais de 2 arrays de n de lado
    ex:   [ [1, 2] ,    [ [2, 6],     =>  [  [3, 8], 
            [3, 4] ]      [3, 9] ]          [6, 13] ]

*/

### Anotações:
callback: chamada de função como parâmetro de outra função

### Tarefa:

Fazer uma função que retorne os alunos com as medias mais altas de cada turma, dada uma coleção de objetos 
EX:
[
    {
        "nome": "Pedro",
        "turma": "A",
        "nota1": 10,
        "nota2": 7
    },
    {
        "nome": "Maria",
        "turma": "B",
        "nota1": 7,
        "nota2": 4
    },
    {
        "nome": "Jonathan",
        "turma": "C",
        "nota1": 9,
        "nota2": 9
    }
]

Resultado:
O aluno Pedro teve a media mais alta da turma A, com 8.5 
O aluno Maria teve a media mais alta da turma B, com 5.5
O aluno Jonathan teve a media mais alta da turma C, com 9
O aluno Jonathan teve a media mais alta da turma C, com 9

