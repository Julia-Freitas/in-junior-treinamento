/*
    Fazer uma função que retorne verdadeiro se um array possui os mesmos valores, independente da ordem
    ex: [1, 1, 2, 3] [2, 3, 1, 1] => true
        [1, 1, 3, 2] [3, 1 , 2, 2] => false
*/