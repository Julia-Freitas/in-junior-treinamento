# Tarefa da aula: Gestão de Tempo - Planejamento

Utilizando um ou mais métodos ensinados durante a aula, desenvolva um planejamento pessoal do dia de hoje, do dia de amanhã, dessa semana e desse mês. O Objetivo é que vocês consigam explicar o planejamento detalhadamente, explicando cada decisão feita. Desta forma, a escolha de formatação do documento é livre. 

Para a avaliação será cobrado:
O método utilizado (GTD, Pomodoro e “rotina”)
Explicação dos planejamentos de hoje, amanhã, semana e mês.

A orientação é que documento do planejamento seja criado  em arquivo de extensão popular, ou seja, docx, xlsx, pdf, txt e afins.
﻿