# Aula 05 - JavaScript dia 1

## Instrutor: Thiago
## Monitores: Arthur Nakao e Rafael Carrilho

### Conteúdo: Basic Concepts e Conditionals and Loops

Indicação para estudo: https://www.sololearn.com/Play/JavaScript/

módulos de aprendizado, explicando tudo e passando exercícios

### Anotações: 

var(variável global) X let(variável local)


### Tarefa :

Fazer um programa que crie uma nova string, transformando os caracteres da string dada ([arquivo de texto](./aulaJs1.txt)) no número deles no alfabeto, usa ponto como 0 e virgula como -1. (não precisa diferenciar letra maiuscula e minuscula)
