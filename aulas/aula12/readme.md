# Aula 12 - POO

Diagrama de classe: draw.io

## Tarefa

Estudar conceitos de POO

- [Slides Leo Murta](http://www.ic.uff.br/~leomurta/courses/2020.1/poo.html)

- [Apostila Caelum POO](https://www.caelum.com.br/apostila-java-orientacao-objetos/)
  
- [Diagrama de Classes](http://www.dsc.ufcg.edu.br/~jacques/cursos/map/html/uml/diagramas/classes/classes3.htm)