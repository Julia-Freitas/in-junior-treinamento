# Treinamento IN Junior

#### [Formulário de Feedback](https://forms.gle/SocTKwSabs3LZY3m7)

#### [Slides das aulas](https://drive.google.com/drive/folders/1jsD13_sbXRR3RNtTQUwShP3aUxOQjpRl?usp=sharing)

#### [Requisitos de instalação](https://docs.google.com/document/d/1OASoP9vlJ7L5u-_AJ1i20VSjORJgXmmX7JxB-_ovqNs/edit)

<table>

<tbody>

<tr>

<th>N</th>

<th>Aula</th>

<th>Repositório</th>

<th>Vídeoaula</th>

<th>Slide</th>

<th>Feedback</th>

<th>Tarefa</th>

<th>Revi o material?</th>

</tr>

<tr>

<td colspan="8"><strong>Semana 1</strong></td>

</tr>

<tr>

<td>01</td>

<td>Formação de Equipes</td>

<td>[Repositório](./aula01)</td>

<td>[Vídeoaula](https://www.youtube.com/watch?v=5XzpPY3bTmQ&feature=youtu.be)</td>

<td>[Slide](https://docs.google.com/presentation/d/1iUDiNPbG9Bu5t-GeO265Z_dzVI674Qg-f3AXJh5R4iI/edit#slide=id.g5d9859a4dd_0_0)</td>

<td>&#9989</td>

<td>&#9989</td>

<td></td>

</tr>

<tr>

<td>02</td>

<td>Git</td>

<td>[Repositório](./aula02)</td>

<td>[Vídeoaula](https://www.youtube.com/watch?v=XkbAnc-Rm2Y&feature=youtu.be)</td>

<td>[Slide](https://docs.google.com/presentation/d/1FjoCSshAh_xDMTR_ExtDJMEoJDW0FIawSqKOBLhUQ58/edit#slide=id.p)</td>

<td>&#9989</td>

<td></td>

<td></td>

</tr>

<tr>

<td>03</td>

<td>Gestão de Tempo</td>

<td>[Repositório](./aula03)</td>

<td>[Vídeoaula](https://www.youtube.com/watch?v=amSjHmnLyFM&feature=youtu.be)</td>

<td>[Slide](https://docs.google.com/presentation/d/1FjoCSshAh_xDMTR_ExtDJMEoJDW0FIawSqKOBLhUQ58/edit#slide=id.p)</td>

<td>&#9989</td>

<td></td>

<td></td>

</tr>

<tr>

<td>04</td>

<td>HTML</td>

<td>[Repositório](./aula04)</td>

<td>[Vídeoaula](https://www.youtube.com/watch?v=4qIcQPF9VIs&feature=youtu.be)</td>

<td>[Slide](https://docs.google.com/presentation/d/1m2VV4AQmRfCeDKnUK0dXHRbqQqAyISTDW9Fiur53G2E/edit#slide=id.g869b14b790_0_93)</td>

<td>&#9989</td>

<td>&#9989</td>

<td></td>

</tr>

<tr>

<td>05</td>

<td>JavaScript - dia 1</td>

<td>[Repositório](./aula05)</td>

<td>[Vídeoaula](https://www.youtube.com/watch?v=4LlMYhgOLNM&feature=youtu.be)</td>

<td>[Slide](https://docs.google.com/presentation/d/1sGcZAdB9Gna7bLreWWvs42g9CWb0lK7WeM5Q4VimSRI/edit?usp=sharing)</td>

<td>&#9989</td>

<td></td>

<td></td>

</tr>

<tr>

<td>06</td>

<td>CSS - dia 1</td>

<td>[Repositório](./aula06)</td>

<td>[Vídeoaula](https://www.youtube.com/watch?v=LxQSMo6_Jqc&feature=youtu.be)</td>

<td>[Slide](https://docs.google.com/presentation/d/1sGcZAdB9Gna7bLreWWvs42g9CWb0lK7WeM5Q4VimSRI/edit?usp=sharing)</td>

<td>&#9989</td>

<td></td>

<td></td>

</tr>

<tr>

<td>07</td>

<td>JavaScript - dia 2</td>

<td>[Repositório](./aula07)</td>

<td>[Vídeoaula](https://www.youtube.com/watch?v=hUxIbR8vJBA&feature=youtu.be)</td>

<td>[Slide](https://docs.google.com/presentation/d/1sGcZAdB9Gna7bLreWWvs42g9CWb0lK7WeM5Q4VimSRI/edit?usp=sharing)</td>

<td>&#9989</td>

<td></td>

<td></td>

</tr>

<tr>

<td>08</td>

<td>CSS - dia 2</td>

<td>[Repositório](./aula08)</td>

<td>[Vídeoaula](https://www.youtube.com/watch?v=47MrZlr5jLg&feature=youtu.be)</td>

<td>[Slide](https://docs.google.com/presentation/d/1sGcZAdB9Gna7bLreWWvs42g9CWb0lK7WeM5Q4VimSRI/edit?usp=sharing)</td>

<td>&#9989</td>

<td></td>

<td></td>

</tr>

<tr>

<td>09</td>

<td>JavaScript - dia 3</td>

<td>[Repositório](./aula09)</td>

<td>[Vídeoaula](https://www.youtube.com/watch?v=zK_4r6gKchw&feature=youtu.be)</td>

<td>[Slide](https://docs.google.com/presentation/d/1sGcZAdB9Gna7bLreWWvs42g9CWb0lK7WeM5Q4VimSRI/edit?usp=sharing)</td>

<td>&#9989</td>

<td></td>

<td></td>

</tr>

<tr>

<td>10</td>

<td>CSS - dia 3</td>

<td>[Repositório](./aula10)</td>

<td>[Vídeoaula](https://www.youtube.com/watch?v=dXaMTJlWW8o&feature=youtu.be)</td>

<td>[Slide](https://docs.google.com/presentation/d/1sGcZAdB9Gna7bLreWWvs42g9CWb0lK7WeM5Q4VimSRI/edit?usp=sharing)</td>

<td>&#9989</td>

<td></td>

<td></td>

</tr>

<tr>

<td colspan="8"><strong>Semana 2</strong></td>

</tr>

<tr>

<td>11</td>

<td>Diretorias IN</td>

<td>[Repositório](./aula11)</td>

<td>[Vídeoaula](https://www.google.com/url?q=https://youtu.be/g4DnWfBhocg&sa=D&source=calendar&usd=2&usg=AOvVaw3kfYW-5iLbBRem5z1BC58a)</td>

<td>[Slide](https://drive.google.com/file/d/1SMjCp4GetFYho0Uwash9mKkX5vyBtlNe/view?usp=sharing)</td>

<td>&#9989</td>

<td> - </td>

<td></td>

</tr>

<tr>

<td>12</td>

<td>POO - Dia 1</td>

<td>[Repositório](./aula12)</td>

<td>[Vídeoaula](https://www.google.com/url?q=https://youtu.be/nCztGRdf6sE&sa=D&source=calendar&usd=2&usg=AOvVaw24j1gDgRKkVZomp-WEwPPk)</td>

<td>[Slide](https://docs.google.com/presentation/d/1_CFAQAzo7Pt3yVukv_SuRe2JfVUA3yyg7I_T0Hmki-A/edit?usp=sharing)</td>

<td></td>

<td></td>

<td></td>

</tr>

<tr>

<td>13</td>

<td>Criação (Figma)</td>

<td>[Repositório](./aula13)</td>

<td>[Vídeoaula](https://www.google.com/url?q=https://youtu.be/uwkm-L8iL40&sa=D&source=calendar&usd=2&usg=AOvVaw1YHegz5T1iRcEWHhusofvr)</td>

<td>[Slide](https://docs.google.com/presentation/d/1sRplFj1dmVeJnXz1cepqhvMayrs9NyV6THn3u5maRW8/edit#slide=id.gc6f80d1ff_0_0)</td>

<td></td>

<td></td>

<td></td>

</tr>

<tr>

<td>14</td>

<td>POO - Dia 2</td>

<td>[Repositório](./aula14)</td>

<td>[Vídeoaula](https://www.google.com/url?q=https://youtu.be/6VQ1nRvSGw0&sa=D&source=calendar&usd=2&usg=AOvVaw13E6s4WODhVul_VQeYtBSb)</td>

<td>[Slide](https://docs.google.com/presentation/d/1_CFAQAzo7Pt3yVukv_SuRe2JfVUA3yyg7I_T0Hmki-A/edit?usp=sharing)</td>

<td></td>

<td></td>

<td></td>

</tr>

<tr>

<td>15</td>

<td>Manipulação da DOM</td>

<td>[Repositório](./aula15)</td>

<td>[Vídeoaula](https://www.google.com/url?q=https://youtu.be/6_pJHiktQzU&sa=D&source=calendar&usd=2&usg=AOvVaw06JregnQXNs7CUw8ViNSTm)</td>

<td>[Slide](https://docs.google.com/presentation/d/1SdKwee4JrxxQ7fJ6jvBX0eadprNide0SFwYGrZRTgMU/edit?usp=sharing)</td>

<td></td>

<td></td>

<td></td>

</tr>

<tr>

<td>16</td>

<td>Rio Junior</td>

<td>[Repositório](./aula16)</td>

<td>[Vídeoaula](https://www.google.com/url?q=https://youtu.be/dCD-Fe830Oo&sa=D&source=calendar&usd=2&usg=AOvVaw0roSIVt2CNlnMRhTPqO8ri)</td>

<td>---</td>

<td></td>

<td></td>

<td></td>

</tr>

<tr>

<td>17</td>

<td>Arquitetura de Aplicações Web</td>

<td>[Repositório](./aula17)</td>

<td>[Vídeoaula](https://www.google.com/url?q=https://youtu.be/7Y-X10XKlig&sa=D&source=calendar&usd=2&usg=AOvVaw1UuLl7LWzaTbdVdb4kiXis)</td>

<td>[Slide](https://docs.google.com/presentation/d/1L1oBn9NZw6B9j41A9BKK66o5sQMzEeeNWfWaq5ZSHLA/edit?usp=sharing)</td>

<td></td>

<td></td>

<td></td>

</tr>

<tr>

<td>18</td>

<td>Ajax e Promises</td>

<td>[Repositório](./aula18)</td>

<td>[Vídeoaula](https://www.google.com/url?q=https://youtu.be/zAsaog4xzC8&sa=D&source=calendar&usd=2&usg=AOvVaw1TNi4ZYA2Nwm7K8sYrFHAM)</td>

<td>[Slide](https://docs.google.com/presentation/d/12LB8yLeozR9EJ-vhELewxewqtdb0J_rJdiFQvrLN_Mk/edit?usp=sharing)</td>

<td></td>

<td></td>

<td></td>

</tr>

<tr>

<td>19</td>

<td>Comercial</td>

<td>[Repositório](./aula19)</td>

<td>[Vídeoaula](https://www.google.com/url?q=https://youtu.be/2HL1Ryr69PI&sa=D&source=calendar&usd=2&usg=AOvVaw3306bb8CPttDV-apIOUKEv)</td>

<td>[Slide]()</td>

<td></td>

<td></td>

<td></td>

</tr>

<tr>

<td>20</td>

<td>Análise</td>

<td>[Repositório](./aula20)</td>

<td>[Vídeoaula](https://www.google.com/url?q=https://youtu.be/qlBIgWYlgR8&sa=D&source=calendar&usd=2&usg=AOvVaw2tK_peqK65SRFGvbI9__9x)</td>

<td>[Slide]()</td>

<td></td>

<td></td>

<td></td>

</tr>

<tr>

<td colspan="8"><strong>Semana 3</strong></td>

</tr>

<tr>

<td>21</td>

<td>Gerência e Soft Skills</td>

<td>[Repositório](./aula21)</td>

<td>[Vídeoaula](https://www.google.com/url?q=https://youtu.be/yB_U0cuz-DI&sa=D&source=calendar&usd=2&usg=AOvVaw2MteM2gwaJKREqanURTqhM)</td>

<td>[Slide]()</td>

<td></td>

<td></td>

<td></td>

</tr>


<tr>

<td>22</td>

<td>WordPress 1</td>

<td>[Repositório](./aula22)</td>

<td>[Vídeoaula](https://www.google.com/url?q=https://youtu.be/iWtfI2PjH4E&sa=D&source=calendar&usd=2&usg=AOvVaw1OPiwJJ-UWlrXbuPTqCKK4)</td>

<td>[Slide]()</td>

<td></td>

<td></td>

<td></td>

</tr>

<tr>

<td>23</td>

<td>Ruby 1</td>

<td>[Repositório](./aula23)</td>

<td>[Vídeoaula](https://www.google.com/url?q=https://youtu.be/VVgohQkBezM&sa=D&source=calendar&usd=2&usg=AOvVaw0-HqDMHAxTxV7uHvpyflRB)</td>

<td>[Slide](https://drive.google.com/file/d/1tzkgenisEeMbRVpJaA2rQsjfvNpo78SM/view?usp=sharing)</td>

<td></td>

<td></td>

<td></td>

</tr>

<tr>

<td>24</td>

<td>WordPress 2</td>

<td>[Repositório](./aula24)</td>

<td>[Vídeoaula](https://www.google.com/url?q=https://youtu.be/vmNtTv8AFTM&sa=D&source=calendar&usd=2&usg=AOvVaw10meWnpfEkV-Ar13VoxTpD)</td>

<td>[Slide]()</td>

<td></td>

<td></td>

<td></td>

</tr>

<tr>

<td>25</td>

<td>Bootstrap</td>

<td>[Repositório](./aula25)</td>

<td>[Vídeoaula](https://www.google.com/url?q=https://youtu.be/nSy10i2FBbQ&sa=D&source=calendar&usd=2&usg=AOvVaw0pcn2DQAaqSatEc_PXV2cT)</td>

<td>[Slide]()</td>

<td></td>

<td></td>

<td></td>

</tr>


<tr>

<td>26</td>

<td>Ruby 2</td>

<td>[Repositório](./aula26)</td>

<td>[Vídeoaula](https://www.google.com/url?q=https://youtu.be/Sm7Eo_9kIrQ&sa=D&source=calendar&usd=2&usg=AOvVaw0k2NDTb2VGJg6OByysA3Xh)</td>

<td>[Slide](https://drive.google.com/file/d/1w8T7AKr6PiMzbaPJgXatfph-E82H6jN0/view?usp=sharing)</td>

<td></td>

<td></td>

<td></td>

</tr>


<tr>

<td>27</td>

<td>WordPress 3</td>

<td>[Repositório](./aula27)</td>

<td>[Vídeoaula](https://www.google.com/url?q=https://youtu.be/R5DSgtHt-uo&sa=D&source=calendar&usd=2&usg=AOvVaw3HblejnxRovWYIyZ6heYax)</td>

<td>[Slide]()</td>

<td></td>

<td></td>

<td></td>

</tr>



<tr>

<td>28</td>

<td>Banco de Dados 1</td>

<td>[Repositório](./aula28)</td>

<td>[Vídeoaula](https://www.google.com/url?q=https://youtu.be/El6bQcCkCJ4&sa=D&source=calendar&usd=2&usg=AOvVaw3x-j7DLOJ1OOHsNz31p2j-)</td>

<td>[Slide]()</td>

<td></td>

<td></td>

<td></td>

</tr>



<tr>

<td>29</td>

<td>WordPress 4</td>

<td>[Repositório](./aula28)</td>

<td>[Vídeoaula](https://www.google.com/url?q=https://youtu.be/0BaNpYwUs7k&sa=D&source=calendar&usd=2&usg=AOvVaw1bPipIkyt0BKihD3MIkyYm)</td>

<td>[Slide]()</td>

<td></td>

<td></td>

<td></td>

</tr>



<tr>

<td>30</td>

<td>Banco de Dados 2</td>

<td>[Repositório](./aula29)</td>

<td>[Vídeoaula](https://www.google.com/url?q=https://youtu.be/F9fKIuUpyqU&sa=D&source=calendar&usd=2&usg=AOvVaw2CR_ybyfr1EcDnfjtVUHJd)</td>

<td>[Slide]()</td>

<td></td>

<td></td>

<td></td>

</tr>


<tr>

<td>31</td>

<td>WordPress 5</td>

<td>[Repositório](./aula30)</td>

<td>[Vídeoaula](https://www.google.com/url?q=https://youtu.be/2uGigtB41S4&sa=D&source=calendar&usd=2&usg=AOvVaw3jcv7nAQMsV1i0P2-OnBTS)</td>

<td>[Slide]()</td>

<td></td>

<td></td>

<td></td>

</tr>

<tr>

<td colspan="8"><strong>Semana 4</strong></td>

</tr>

<tr>

<td>32</td>

<td>React 1</td>

<td>[Repositório](./aula32)</td>

<td>[Vídeoaula]()</td>

<td>[Slide]()</td>

<td></td>

<td></td>

<td></td>

</tr>

<tr>

<td>33</td>

<td>React 2</td>

<td>[Repositório](./aula33)</td>

<td>[Vídeoaula]()</td>

<td>[Slide]()</td>

<td></td>

<td></td>

<td></td>

</tr>

<tr>

<td>34</td>

<td>React 3</td>

<td>[Repositório](./aula34)</td>

<td>[Vídeoaula]()</td>

<td>[Slide]()</td>

<td></td>

<td></td>

<td></td>

</tr>

<tr>

<td>35</td>

<td>React 4</td>

<td>[Repositório](./aula35)</td>

<td>[Vídeoaula]()</td>

<td>[Slide]()</td>

<td></td>

<td></td>

<td></td>

</tr>

<tr>

<td>36</td>

<td>React 5</td>

<td>[Repositório](./aula36)</td>

<td>[Vídeoaula]()</td>

<td>[Slide]()</td>

<td></td>

<td></td>

<td></td>

</tr>

<tr>


<td>37</td>

<td>React 6</td>

<td>[Repositório](./aula37)</td>

<td>[Vídeoaula]()</td>

<td>[Slide]()</td>

<td></td>

<td></td>

<td></td>

</tr>

<tr>

<td>38</td>

<td>React 7</td>

<td>[Repositório](./aula38)</td>

<td>[Vídeoaula]()</td>

<td>[Slide]()</td>

<td></td>

<td></td>

<td></td>

</tr>

<tr>

<td>39</td>

<td>React 8</td>

<td>[Repositório](./aula39)</td>

<td>[Vídeoaula]()</td>

<td>[Slide]()</td>

<td></td>

<td></td>

<td></td>

</tr>

<tr>

<td>40</td>

<td>React 9</td>

<td>[Repositório](./aula40)</td>

<td>[Vídeoaula]()</td>

<td>[Slide]()</td>

<td></td>

<td></td>

<td></td>

</tr>

<tr>

<td colspan="8"><strong>Semana 5</strong></td>

</tr>

<tr>

<td>41</td>

<td>Deploy</td>

<td>[Repositório](./aula41)</td>

<td>[Vídeoaula]()</td>

<td>[Slide]()</td>

<td></td>

<td></td>

<td></td>

</tr>

<tr>

<td>42</td>

<td>Deploy</td>

<td>[Repositório](./aula42)</td>

<td>[Vídeoaula]()</td>

<td>[Slide]()</td>

<td></td>

<td></td>

<td></td>

</tr>

</tbody>

</table>
