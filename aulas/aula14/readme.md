# Aula 14 - POO - Dia 2

### Referencias
[Diagrama de Classe](https://docente.ifrn.edu.br/givanaldorocha/disciplinas/engenharia-de-software-licenciatura-em-informatica/diagrama-de-classes)


### Tarefa

fazer o diagrama de classes para atender as necessidades do iduff.

Requisitos:
Alunos se inscreverem nas turmas, onde cada turma tem uma matéria, um professor e horário da aula

Matérias equivalentes: No iduff tem matérias que podem substituir outras matérias por terem assuntos semelhantes. ex: Programação estruturada e programaçao 2

Professores só podem dar aulas nas matérias que são capacitados. ex: professor capacitado em matematica e calculo não pode dar aula de gestão de pessoas
editado

O trabalho deve ser entregue até as 23:59 de hj, pelo gitlab, com preferencia no formato de .png ou pdf, qualquer duvida pode falar comigo. Boa sorte!!!