# Aula 28 - Banco de Dados 1


### Material da aula de Banco de Dados 1:
#### Resumo de relações do MER:
 - ATENDE-A (1:N) entre ALUNO e AULA.
 - LECIONA (1:N) entre DISCIPLINA e PROFESSOR.
 - PERTENCE (1:N) entre DISCPLINA e AULA. A participação de AULA é total.
 - PODE-DAR (1:N) entre PROFESSOR e AULA. A participação de AULA é total
 - GERA (1:N) entre DISCPLINA e NOTA. A participação de NOTA é total
 - TEM (1:N) entre NOTA e ALUNO. A participação de NOTA é total.
 - MINISTRA (1:N) entre ALUNO, AULA e PROFESSOR.

#### Fontes:
https://www.ime.usp.br/~jef/apostila.pdf
https://medium.com/@felipeozalmeida/guia-da-modelagem-de-dados-introdu%C3%A7%C3%A3o-modelo-conceitual-238c1f8be

--------------------------------------------------------------------------------


Exemplo de Aula 

Sistema de gerenciamentos de atividades e pessoal de uma escola. É necessário realizar cadastro de disciplinas, de professores, de aulas e de alunos.
Cada disciplina tem as seguintes informações: código único, carga horária, créditos, turno e o tema.
Cada professor tem as seguintes informações: uma matrícula, endereço com rua, número e bairro, nome com primeiro e último nomes, salário, data de nascimento e sexo.
Cada aluno tem as seguintes informações: nome com primeiro e último nomes, uma matrícula, endereço com rua, número e bairro, data de nascimento, idade e sexo.
Cada aula tem as seguintes informações: carga horária, código único, lista presença e horário.
Cada professor dá aulas da disciplina que ao qual ele leciona. Alunos assistem às aulas e recebem notas em cada disciplina.


![Diagrama do Exemplo1](DER.jpg)